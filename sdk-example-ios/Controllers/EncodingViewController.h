//
//  EncodingViewController.h
//  CryptoDisk
//
//  Created by Alex on 15.02.16.
//
//

#import <UIKit/UIKit.h>

@class YDSession;

typedef enum
{
    EncodingTypeEncode,
    EncodingTypeDecode
} EncodingType;

@interface EncodingViewController : UIViewController

@property (nonatomic) EncodingType controllerEncodingType;

@property (nonatomic, strong) UIImage* assetImage;
@property (nonatomic, strong) NSURL* referenceURL;
@property(nonatomic, copy) NSString *directoryPath;
@property(nonatomic, copy) NSString *imageName;

@end
