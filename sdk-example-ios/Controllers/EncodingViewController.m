//
//  EncodingViewController.m
//  CryptoDisk
//
//  Created by Alex on 15.02.16.
//
//
#import "AppDelegate.h"

#import "EncodingViewController.h"
#import "DirectoryViewController.h"

#import "YDSession.h"
#import "YDConstants.h"

#import "CryptoDisk-Swift.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface EncodingViewController () <UITextFieldDelegate>
{
    __weak IBOutlet UIView *loadingView;
    __weak IBOutlet UIProgressView *progressView;
    __weak IBOutlet UILabel *progressLabel;

    __weak IBOutlet UITextField *passwordTextField;

    __weak IBOutlet UIButton *doneButton;
    __weak IBOutlet UIImageView *resultImageView;
    
    __weak IBOutlet UILabel *instructionlabel;
    __weak IBOutlet UILabel *descriptionLabel;

    YDSession *session;
    NSString* passwordData;

    BOOL processStarted;
}
@end

@implementation EncodingViewController

#pragma mark - View life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    session = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).session;
    processStarted = NO;

    [self setupType];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kYDSessionDidSendPartialDataForFileNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];

    [passwordTextField becomeFirstResponder];
}

#pragma mark - Methods

-(void)setupType
{
    loadingView.hidden = YES;
    doneButton.enabled = NO;

    if (self.controllerEncodingType == EncodingTypeEncode)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateProgressVisibility:)
                                                     name:kYDSessionDidSendPartialDataForFileNotification
                                                   object:nil];
    }
    else if (self.controllerEncodingType == EncodingTypeDecode)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateProgressVisibility:)
                                                     name:kYDSessionDidGetPartialDataForFileNotification
                                                   object:nil];

        instructionlabel.text = @"Пожалуйста введите пароль для расшифровки";
        descriptionLabel.text = @"Качаем и декодируем";
    }
}

- (IBAction)closeButtonPressed
{
    [passwordTextField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneButtonPressed
{
    [passwordTextField resignFirstResponder];

    processStarted = YES;
    loadingView.hidden = NO;

    doneButton.enabled = NO;

    progressLabel.text = @"0%";
    progressView.progress = 0.f;

    if (self.controllerEncodingType == EncodingTypeEncode)
    {
        [self startDataUpload];
    }
    else if (self.controllerEncodingType == EncodingTypeDecode)
    {
        [self startDataDownload];
    }
}

-(void)updateProgressVisibility:(NSNotification*)notification
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        NSDictionary* userInfo = notification.userInfo;
        float ratio = 0;

        if (self.controllerEncodingType == EncodingTypeEncode)
        {
            NSUInteger totalSend = [[userInfo objectForKey:@"totalSent"] unsignedIntegerValue];
            NSUInteger totalExpected = [[userInfo objectForKey:@"totalExpected"] unsignedIntegerValue];

            ratio = (float)totalSend / (float)totalExpected;
        }
        else if (self.controllerEncodingType == EncodingTypeDecode)
        {
            NSUInteger totalReceived = [[userInfo objectForKey:@"receivedDataLength"] unsignedIntegerValue];
            NSUInteger totalExpected = [[userInfo objectForKey:@"expectedDataLength"] unsignedIntegerValue];

            ratio = (float)totalReceived / (float)totalExpected;
        }

        progressLabel.text = [NSString stringWithFormat:@"%2.f\%%",ratio * 100.f];
        progressView.progress = ratio;
    });
}

-(void)startDataUpload
{
    // define the block to call when we get the asset based on the url (below)
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        NSString* fileName =  [[imageRep filename] stringByDeletingPathExtension];

        NSData *imageData = UIImagePNGRepresentation(self.assetImage);

        // Generate the file path
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.crypt", fileName]];

        // Save it into file system
        [[self encryptData:imageData] writeToFile:dataPath atomically:YES];

        [session uploadFile:dataPath toPath:[NSString stringWithFormat:@"%@/%@.crypt", self.directoryPath, fileName] completion:^(NSError *err)
         {
             if (!err)
             {
                 dispatch_async(dispatch_get_main_queue(), ^
                 {
                     UINavigationController* contr = (UINavigationController*)self.presentingViewController;
                     if ([contr.topViewController isKindOfClass:[DirectoryViewController class]])
                     {
                         [((DirectoryViewController*)contr.topViewController) loadDir];
                     }
                     [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                 });
             }
             else
             {
                 NSLog(@"ERROR:%@", err);
             }
         }];
    };

    // get the asset library and fetch the asset based on the ref url (pass in block above)
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:self.referenceURL resultBlock:resultblock failureBlock:nil];
}

-(void)startDataDownload
{
    // Generate the file path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:self.imageName];

    [session downloadFileFromPath:self.directoryPath toFile:dataPath completion:^(NSError *err)
     {
         if (!err)
         {
             dispatch_async(dispatch_get_main_queue(), ^
             {
                 NSData *data = [[NSFileManager defaultManager] contentsAtPath:dataPath];
                 NSData* decodedData = [self decryptData:data];
                 UIImage* decodedImage = [UIImage imageWithData:decodedData];
                 resultImageView.image = decodedImage;
                 resultImageView.hidden = NO;
                 [resultImageView layoutIfNeeded];
             });
         }
         else
         {
             NSLog(@"ERROR:%@", err);
         }
     }];
}


-(NSData*)encryptData:(NSData*)data
{
    NSString *password = passwordTextField.text;//@"2738213e755f5a665e444c253d4b6132353874464a3070385c2b504122";
    NSData *cipherData = [RNCryptor encryptData:data password:password];

    return cipherData;
}

-(NSData*)decryptData:(NSData*)data
{
    NSString *password = passwordTextField.text;//@"2738213e755f5a665e444c253d4b6132353874464a3070385c2b504122";

    NSError *error = nil;
    NSData *trueData = [RNCryptor decryptData:data password:password error:&error];
    if (error != nil)
    {
        NSLog(@"ERROR:%@", error);
        return nil;
    }

    return trueData;
}

#pragma mark - UITextFieldDelegate

- (IBAction)passTextFieldDidChange
{
    if (passwordTextField.text.length > 5)
    {
        doneButton.enabled = YES;
    }
    else
    {
        doneButton.enabled = NO;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (processStarted)
        return NO;
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
