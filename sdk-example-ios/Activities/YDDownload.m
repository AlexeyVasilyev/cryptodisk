

#import "YDDownload.h"
#import "YDSession.h"

@interface YDDownload()

@property(strong, nonatomic) NSMutableArray * files;

@end


@implementation YDDownload

- (UIImage *)activityImage
{
    static UIImage *image = nil;
    if (!image) image = [UIImage imageNamed:@"Download_icon"];
    return image;
}

- (NSString *)activityTitle
{
    return @"Download";
}

- (NSString *)activityType
{
    return @"crypto.disk.download";
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    for (id element in activityItems)
    {
        if ([[element class] isSubclassOfClass:NSClassFromString(@"YDItemStat")])
        {
            YDItemStat * item = element;
            NSString* objName = item.name;
            NSString* ext = [objName pathExtension];
            if ([ext isEqualToString:@"crypt"])
            {
                return YES;
            }
        }
    }
    return NO;
}

- (void)performActivity
{
    for (YDItemStat *item in self.files)
    {
        [self activityDidFinish:YES];
    }
}

- (void)prepareWithActivityItems:(NSArray *)activityItems
{
    self.files = [[NSMutableArray alloc] init];
    for (id element in activityItems)
    {
        if ([[element class] isSubclassOfClass:NSClassFromString(@"YDItemStat")])
        {
            [self.files addObject:element];
        }
    }
}

@end
